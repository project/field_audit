<?php

/**
 * Field audit report page.
 */
function field_audit_report() {
	$instances = field_info_instances();
  $field_types = field_info_field_types();
  $bundles = field_info_bundles();
  $entities = entity_get_info();
  $modules = system_rebuild_module_data();
  $result = array();

  $header = array(t('Bundle'), t('Field'), t('Notes'));
  if(module_exists('feeds')) {
  	$header[] = t('Feeds');
  	$feeds = feeds_importer_load_all(TRUE);
  	$processors = FeedsPlugin::byType('processor');
  	$feed_entities = array();
  	foreach($feeds as $feed_name => $feed) {
  		$config = $feed->getConfig();
  		$processor = $processors[$config['processor']['plugin_key']];
  		$instance = feeds_plugin($processor['handler']['class'], '__none__');
  		$entity_type = $instance->entityType();
  		$feed_entities[$entity_type][$feed_name] = $config;
  	}
  }
  $field_audit = array();
  $return = array('form' => drupal_get_form('field_audit_report_form'));
  $filter = ($_SESSION['field_audit_report']['entity_type']) ? $_SESSION['field_audit_report']['entity_type'] : FALSE;
  foreach ($instances as $entity_type => $type_bundles) {
    foreach ($type_bundles as $bundle => $bundle_instances) {
      if(!$filter || isset($filter[$entity_type . '-all']) ||
      	 isset($filter[$entity_type . '-all']) ||
         isset($filter[$entity_type . '-' . $bundle])) {
	      foreach ($bundle_instances as $field_name => $instance) {
	        $field = field_info_field($field_name);
	        $field_audit[$entity_type][$bundle][$field_name] = $instance;
	      }
	    }
    }
  }
  foreach($field_audit as $entity_type => $bundles) {
  	$return[$entity_type] = array(
  		'#prefix' => '<h3>',
  		'#suffix' => '</h3>',
  		'#markup' => $entities[$entity_type]['label'],
  		);
  	$rows = array();
  	foreach($bundles as $bundle_name => $bundle) {
  		foreach($bundle as $field_name => $field) {
  			$row = array($entities[$entity_type]['bundles'][$bundle_name]['label'],
  											$field['label'],
  											isset($field['field_audit_note']) ?
  												check_plain($field['field_audit_note']) :
  												''
  									);
  			if(module_exists('feeds')) {
  				$importers = array();
  				foreach($feed_entities[$entity_type] as $feed) {
  					foreach($feed['processor']['config']['mappings'] as $mapping) {
  						if($mapping['target'] == $field_name) {
  							$importers[] = t('Feed %feed, source %source', array('%feed' => $feed['name'], '%source' => $mapping['source']));
  						}
  					}
  				}
  				$row[] = (count($importers)) ?
  								  theme('item_list', array('items' => $importers)) :
  								  '';
  			}
  			$rows[] = $row;
  		}
  	}
  	$return[$entity_type . '_table'] = array(
  		'#markup' => theme('table', array('header' => $header,
  													'rows' => $rows))
  		);
  }

  
  return $return;
}

/**
 * Form handler for filtering the audit report.
 */
function field_audit_report_form() {
	$form = array();

	$entities = entity_get_info();
  $entity_options = array();
  foreach($entities as $entity_type => $entity) {
  	$entity_options[$entity['label']] = array($entity_type . '-all' => t('Any @type', array('@type' => $entity['label'])));
  	foreach($entity['bundles'] as $bundle_name => $bundle) {
  		$entity_options[$entity['label']][$entity_type . '-' . $bundle_name] = $bundle['label'];
  	}
  }
	$form['entity_type'] = array(
		'#type' => 'select',
		'#title' => t('Filter by entity type'),
		'#options' => $entity_options,
		'#multiple' => TRUE,
		'#default_value' => (isset($_SESSION['field_audit_report']['entity_type'])) ? $_SESSION['field_audit_report']['entity_type'] : null
		);

	$form['actions'] = array(
		'#type' => 'actions',
		);

	$form['actions']['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Filter',
		);

	$form['actions']['reset'] = array(
		'#type' => 'button',
		'#value' => 'Reset',
		);
	return $form;
}

/**
 * Submit handler for filtering the field audit report.
 */
function field_audit_report_form_submit($form, $form_state) {
	$_SESSION['field_audit_report'] = array();
	if($form_state['triggering_element']['#type'] == 'submit') {
		$_SESSION['field_audit_report']['entity_type'] = $form_state['values']['entity_type'];
  }
}